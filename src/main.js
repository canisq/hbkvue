import Vue from 'vue'
import App from 'components/App'

import router from './router'
import store from './store'
import { sync } from 'vuex-router-sync'
import * as filters from './filters'

// register global utility filters.
Object.keys(filters).forEach(key => {
  Vue.filter(key, filters[key])
})

// keep vue-router and vuex store in sync.
// this registers `store.state.route`
sync(store, router)

// create the app instance.
const app = new Vue({
  el: '#app',
  router,
  store,
  render: r => r(App)
})

global.HBKV = app

// expose the app, the router and the store.
// note we are not mounting the app here, since bootstrapping will be
// different depending on whether we are in a browser or on the server.
export { app, router, store }
