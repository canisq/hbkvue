import axios from 'axios'

const paths = {
  fetchProducts: '/api/products',
  fetchInspirations: '/api/photos',
  fetchArticles: '/api/articles',
  fetchDiscussions: 'api/discussions',
  fetchProfessionals: 'api/professionals',
  fetchUsers: 'api/users'
}

export default {
  fetchProducts (params) {
    return axios.get(paths.fetchProducts, { params })
  },
  fetchInspirations (params) {
    return axios.get(paths.fetchInspirations, { params })
  },
  fetchArticles (params) {
    return axios.get(paths.fetchArticles, { params })
  },
  fetchDiscussions (params) {
    return axios.get(paths.fetchDiscussions, { params })
  },
  fetchProfessionals (params) {
    return axios.get(paths.fetchProfessionals, { params })
  },
  fetchUsers (params) {
    return axios.get(paths.fetchUsers, { params })
  }
}
