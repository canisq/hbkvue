import Vue from 'vue'
import VueRouter from 'vue-router'

Vue.use(VueRouter)

function lazy (component) {
  return r => require([`components/${component}`], r)
}

export default new VueRouter({
  mode: 'history',
  base: __dirname,
  scrollBehavior: () => ({ y: 0 }),
  linkActiveClass: 'is-active',
  routes: [{
    name: 'products',
    path: '/produkty/:categoryId(\\d+)?', // (marka-):brandId(\\d+)?
    component: lazy('products/ProductView')
  }, {
    name: 'product',
    path: '/produkt',
    component: lazy('products/ProductView')
  }, {
    name: 'inspirations',
    path: '/inspiracje/:categoryId(\\d+)?/:styleId(\\d+)?/',
    component: lazy('inspirations/InspirationView')
  }, {
    name: 'articles',
    path: '/artykuly/:categoryId(\\d+)?/',
    component: lazy('articles/ArticleView')
  }, {
    name: 'specialists',
    path: '/specjalisci',
    component: lazy('products/ProductView')
  }, {
    name: 'forum',
    path: '/forum',
    component: lazy('products/ProductView')
  }, {
    name: 'zaloguj',
    path: '/login',
    redirect: '/produkty'
  }, {
    name: 'home',
    path: '/',
    redirect: '/products'
  },
  {
    path: '*',
    redirect: '/'
  }]
})
