import Vue from 'vue'
import Vuex from 'vuex'

// import { fetchProducts } from 'src/api'

Vue.use(Vuex)

const state = {
  loading: false
}

const mutations = {
  LOADING (state) {
    state.loading = true
  },
  LOADED (state) {
    state.loading = false
  }
}

const actions = {
  loading: ({ commit }) => {
    commit('LOADING')
  },
  loaded: ({ commit }) => {
    commit('LOADED')
  }
}

export default new Vuex.Store({
  strict: process.env.NODE_ENV !== 'production',
  state,
  mutations,
  actions
})
